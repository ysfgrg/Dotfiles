#!/bin/sh 

cd ~/
cp -r ~/.dotfiles/.zshenv ~/
cp -r ~/.dotfiles/.gitconfig ~/
cp -r ~/.dotfiles/.config/picom ~/.config
cp -r ~/.dotfiles/.config/alacritty ~/.config
cp -r ~/.dotfiles/.config/dunst ~/.config
cp -r ~/.dotfiles/.config/ranger ~/.config
cp -r ~/.dotfiles/.config/zsh ~/.config
cp -r ~/.dotfiles/.local/share/themes ~/.local/share
cp -r ~/.dotfiles/.local/share/icons ~/.local/share
